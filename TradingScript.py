import logging
import time
import numpy as np
import pandas as pd
import sklearn
from decimal import Decimal
from hummingbot.core.event.events import TradeFee
from hummingbot.strategy.arbitrage import ArbitrageStrategy
from hummingbot.strategy.stop_loss import StopLossOrderManagementDelegate
from hummingbot.strategy.position_management import PositionManagementDelegate
from hummingbot.connector.exchange.binance.binance_constants import Constants

from hummingbot.strategy.market_trading_pair_tuple import MarketTradingPairTuple
from hummingbot.strategy.pure_market_making import PureMarketMakingStrategy
from hummingbot.strategy.asset_price_delegate import AssetPriceDelegate #Just experimenting with these
from hummingbot.strategy.cross_exchange_market_making import CrossExchangeMarketMakingStrategy


logging.basicConfig(level=logging.INFO)

trading_pair = "ETH-USDT"


exchange_a = "binance" #IDK when backtesting, it's not trading from binance but solely binance paper trade? Maybe paramters for arbritrage were too low or something?

market_a = MarketTradingPairTuple( #Eventually, do not implement AMM arbritrage. I keep executing but it's not trading between markets for some reason. 
    trading_pair,
    exchange_a,
    base_asset_precision=17, #Can change these numbers, seem to have impact on returns. 
    quote_asset_precision=4,
    min_order_size=0.001 #Reddit loves 0.001, even while backtesting. IDK why? I'm gonna stick with it tho
)

exchange_b = "binance_paper_trade"

market_b = MarketTradingPairTuple(
    trading_pair,
    exchange_b,
    base_asset_precision=8,
    quote_asset_precision=2,
    min_order_size=0.001 #This looks like setup, I'm not sure how this is exactly different from the 
)


trading_pair_2 = "BTC-USDT"


market_making_strategy = PureMarketMakingStrategy(market_infos=[market_a, market_b])
market_making_strategy.add_parameter("bid_spread", 0.034) #Messing around 
market_making_strategy.add_parameter("ask_spread", 0.082)
market_making_strategy.add_parameter("order_levels", 3) #I keep on seeing three here, not sure why but I'll keep it constant. 
market_making_strategy.add_parameter("order_level_spread", 0.001)
market_making_strategy.add_parameter("order_level_amount", 0.001)


asset_price_delegate = AssetPriceDelegate()
trend_following_strategy = CrossExchangeMarketMakingStrategy(
    market_infos=[market_a, market_b],
    asset_price_delegate=asset_price_delegate,
    bid_spread=Decimal("0.007"),
    ask_spread=Decimal("0.008"),
    order_levels=3,
    order_level_spread=Decimal("0.002"),
    order_level_amount=Decimal("0.001"),
    minimum_spread=Decimal("0.005"),
    price_ceiling=None,
    price_floor=None,
    price_floor_factor=None,
    price_ceiling_factor=None,
    price_floor_buffer=None,
    price_ceiling_buffer=None,
    order_refresh_time=30,
    cancel_order_wait_time=30,
    hanging_orders_enabled=False,
    hanging_orders_cancel_pct=Decimal("0.02")
)

# Arbritrage but not using it anymore
arbitrage_strategy = ArbitrageStrategy(market_a_info=market_a, market_b_info=market_b)
arbitrage_strategy.add_parameter("ask_order_refresh_time", 20)
arbitrage_strategy.add_parameter("bid_order_refresh_time", 20) #Reducing this because everytime u ask for a refresh it costs money 
arbitrage_strategy.add_parameter("minimum_profitability", 0.010) #Does arbitrage make you guaranteed money???

# Basic setup found on docs and reddit, not sure how these impact overall trading strategy. 
stop_loss_order_management_delegate = StopLossOrderManagementDelegate()
stop_loss_order_management_delegate.add_market_info(market_info=market_a, stop_loss_pct=Decimal("0.02"))
stop_loss_order_management_delegate.add_market_info(market_info=market_b, stop_loss_pct=Decimal("0.02"))
position_management_delegate = PositionManagementDelegate()
position_management_delegate.add_market_info(market_info=market_a, max_long_position=Decimal("0.05"), max_short_position=Decimal("-0.05"))
position_management_delegate.add_market_info(market_info=market_b, max_long_position=Decimal("0.05"), max_short_position=Decimal("-0.05"))


strategy = market_making_strategy
strategy.add_child_strategy(trend_following_strategy)
strategy.add_order_management_delegate(stop_loss_order_management_delegate)
strategy.add_position_management_delegate(position_management_delegate)


#Line that executes it. 
strategy.start()
